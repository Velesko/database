﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Database
{
   
    public partial class Form2 : Form
    {

        public bool button2WasClicked = false; //úprava - delete
        public bool button3WasClicked = false; //výpis
        public bool button4WasClicked = false; //vyhledávání
        public bool button5WasClicked = false; //ukládání
        public double beta;
        public double alpha;
        public double ro;
        
        public Form2(string UserName)
        {
            InitializeComponent();
            textBox12.Visible = false;

            textBox12.Text = UserName;
            label12.Text = UserName;

            button1.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;


            label11.Visible = false;
            textBox11.Visible = false;
            button7.Visible = false;
            button6.Visible = false;
            textBox6.Visible = false;
            textBox7.Visible = false;
            textBox8.Visible = false;
            textBox9.Visible = false;
            textBox10.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;

            dataGridView1.Visible = true;
            button8.Visible = true;
            button10.Visible = false;
            dataGridView3.Visible = false;
            button9.Visible = false;
            dataGridView2.Visible = false;

            textBox13.Visible = false;
            label14.Visible = false;

            prvek.Visible = false;
            label15.Visible = false;
            teplota.Visible = false;
            label16.Visible = false;
            vyska.Visible = false;
            label17.Visible = false;
            sirka.Visible = false;
            label18.Visible = false;
            delka.Visible = false;
            odpor.Visible = false;
            zmena_obemu.Visible = false;
            zmena_hustoty.Visible = false;
            vaha.Visible = false;
            button11.Visible = false;
        }
        DataTable dbdataset2;
        // textBox12 - uživatel

        //logout
        public void end_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Close();
            this.Dispose();
        }
/////////            menu
/**/    public void button5_Click(object sender, EventArgs e)
/**/    {
/**/        button2WasClicked = false;
/**/        button3WasClicked = false;
/**/        button4WasClicked = false;
/**/        button5WasClicked = true;
/**/        blop();
/**/   }
/**/
/**/    public void button2_Click(object sender, EventArgs e)
/**/    {
/**/        button2WasClicked = true;
/**/        button3WasClicked = false;
/**/        button4WasClicked = false;
/**/        button5WasClicked = false;
/**/        blop();
/**/    }
/**/
/**/    public void button3_Click(object sender, EventArgs e)
/**/    {
/**/        button2WasClicked = false;
/**/        button3WasClicked = true;
/**/        button4WasClicked = false;
/**/        button5WasClicked = false;
/**/        blop();
/**/    }
/**/
/**/    public void button4_Click(object sender, EventArgs e)
/**/    {
/**/        button2WasClicked = false;
/**/        button3WasClicked = false;
/**/        button4WasClicked = true;
/**/        button5WasClicked = false;
/**/        blop();
/**/    }
//////////         konec menu
        
        //interakce
        public void blop() {
            if (button5WasClicked)
            {
                button5.BackColor = Color.Lime;
                button1.Visible = true;
                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;
                label1.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                label5.Visible = true;
            }
            else {
                button5.BackColor = DefaultBackColor;
                button1.Visible = false;
                textBox1.Visible = false;
                textBox2.Visible = false;
                textBox3.Visible = false;
                textBox4.Visible = false;
                textBox5.Visible = false;
                label1.Visible = false;
                label2.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                label5.Visible = false;
            }
            // výpis
            if (button3WasClicked)
            {
                button3.BackColor = Color.Lime;
                button8.Visible = true;
                dataGridView1.Visible = true;
            }
            else {
                button3.BackColor = DefaultBackColor;
                button8.Visible = false;
                dataGridView1.Visible = false;
            }
            // vyhledávání
            if (button4WasClicked)
            {
                button4.BackColor = Color.Lime;
                button9.Visible = true;
                dataGridView2.Visible = true;
                textBox13.Visible = true;
                label14.Visible = true;
                prvek.Visible = true;
                label15.Visible = true;
                teplota.Visible = true;
                label16.Visible = true;
                vyska.Visible = true;
                label17.Visible = true;
                sirka.Visible = true;
                label18.Visible = true;
                delka.Visible = true;
                odpor.Visible = true;
                zmena_obemu.Visible = true;
                zmena_hustoty.Visible = true;
                vaha.Visible = true;
                button11.Visible = true;
            }
            else {
                button4.BackColor = DefaultBackColor;
                button9.Visible = false;
                dataGridView2.Visible = false;
                textBox13.Visible = false;
                label14.Visible = false;
                prvek.Visible = false;
                label15.Visible = false;
                teplota.Visible = false;
                label16.Visible = false;
                vyska.Visible = false;
                label17.Visible = false;
                sirka.Visible = false;
                label18.Visible = false;
                delka.Visible = false;
                odpor.Visible = false;
                zmena_obemu.Visible = false;
                zmena_hustoty.Visible = false;
                vaha.Visible = false;
                button11.Visible = false;
            }
            // úprava + odstraňování objektů
            if (button2WasClicked)
            {
                button2.BackColor = Color.Lime;
                button10.Visible = true;
                dataGridView3.Visible = true;
                label11.Visible = true;
                textBox11.Visible = true;
                button7.Visible = true;
                button6.Visible = true;
                textBox6.Visible = true;
                textBox7.Visible = true;
                textBox8.Visible = true;
                textBox9.Visible = true;
                textBox10.Visible = true;
                label6.Visible = true;
                label7.Visible = true;
                label8.Visible = true;
                label9.Visible = true;
                label10.Visible = true;
            }
            else {
                button2.BackColor = DefaultBackColor;
                button10.Visible = false;
                dataGridView3.Visible = false;
                label11.Visible = false;
                textBox11.Visible = false;
                button7.Visible = false;
                button6.Visible = false;
                textBox6.Visible = false;
                textBox7.Visible = false;
                textBox8.Visible = false;
                textBox9.Visible = false;
                textBox10.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                label8.Visible = false;
                label9.Visible = false;
                label10.Visible = false;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        /**
         * globální proměnné
         **/
        public static class globalVar
        {
            public const string db_Name = "localhost";
            public const string table_Name = "testtor";
            public const string user_Name = "root";
            public const string user_Pass = "toor";
        }
        //ukládání
        private void button1_Click(object sender, EventArgs e)
        {
            string constring = "data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass;
            string Query = "insert into " + globalVar.table_Name + ".data (prvek,beta,alpha,ro,kod,uzivatel) value('" + this.textBox1.Text + "','" + this.textBox2.Text + "','" + this.textBox3.Text + "','" + this.textBox4.Text + "','" + this.textBox5.Text + "','" + this.textBox12.Text + "')";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("Uloženo!");
                while (myReader.Read()) { 
                    
                }

            }catch(Exception ex){
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conDataBase.Close();
            }
            }
        //úprava
        public class Prvek
        {
            public string ID;
            public string prvek;
            public string beta;
            public string alpha;
            public string ro;
            public string kod;
            public string uzivatel;
        }

        public class Uzivateltest
        {
            public string ID;
            public string jmeno;
            public string heslo;
            public int pravomoce;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //převod informací z databáze do listu

            List<Prvek> prvek = new List<Prvek>();
            var value = this.textBox11.Text;
            if (String.IsNullOrEmpty(value))
            {
                MessageBox.Show("Nevybrali jste žádný záznam!");
            }
            else 
            {
                MySqlConnection connect = new MySqlConnection("data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass);
                MySqlCommand comm = new MySqlCommand("select * from "+globalVar.table_Name+".data where ID='" + this.textBox11.Text + "';", connect);
                MySqlDataReader dare;
                try
                {
                    connect.Open();
                    dare = comm.ExecuteReader();
                    while (dare.Read())
                    {
                        prvek.Add(new Prvek()
                        {
                            ID = dare.GetString("ID"),
                            prvek = dare.GetString("prvek"),
                            beta = dare.GetString("beta"),
                            alpha = dare.GetString("alpha"),
                            ro = dare.GetString("ro"),
                            kod = dare.GetString("kod"),
                            uzivatel = dare.GetString("uzivatel"),
                        });
                    }

                }

                catch
                {
                    throw;
                }
                finally 
                {
                    connect.Close();
                }
            }
            // zjištění pozice přihlášeného uživatele (administrátor / uživatel druhé úrovně)
            List<Uzivateltest> uzivateltest = new List<Uzivateltest>();
            MySqlConnection connection = new MySqlConnection("data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass);
            MySqlCommand command = new MySqlCommand("select * from "+globalVar.table_Name+".uzivatele where jmeno='" + this.textBox12.Text + "' ;", connection);
            MySqlDataReader darek;
            try
            {
                connection.Open();
                darek = command.ExecuteReader();
                while (darek.Read())
                {
                    uzivateltest.Add(new Uzivateltest()
                    {
                        ID = darek.GetString("ID"),
                        jmeno = darek.GetString("jmeno"),
                        heslo = darek.GetString("heslo"),
                        pravomoce = darek.GetInt32("pravomoce"),
                    });
                }

            }

            catch
            {
                throw;
            }
            finally
            {
                connection.Close();
            }

            if ((prvek[0].uzivatel == this.textBox12.Text) || (uzivateltest[0].pravomoce == 2))
            {
                var val = this.textBox11.Text;
                if (String.IsNullOrEmpty(val))
                {
                    MessageBox.Show("Nevybrali jste žádný záznam!");
                }
                else
                {
                    string constring = "data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass;
                    string Query = "update "+globalVar.table_Name+".data set prvek='" + this.textBox10.Text + "',beta='" + this.textBox9.Text + "',alpha='" + this.textBox8.Text + "',ro='" + this.textBox7.Text + "',kod='" + this.textBox6.Text + "' where ID='" + this.textBox11.Text + "';";
                    MySqlConnection conDataBase = new MySqlConnection(constring);
                    MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
                    MySqlDataReader myReader;
                    try
                    {
                        conDataBase.Open();
                        myReader = cmdDataBase.ExecuteReader();
                        MessageBox.Show("Upraveno!");
                        while (myReader.Read())
                        {

                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        conDataBase.Close();
                    }
                }
            }
            else
                MessageBox.Show("Na tohle nemáte oprávnění!!!!");
        }
        
        // delete button
        private void button7_Click(object sender, EventArgs e)
        {
            ///
            List<Uzivateltest> uzivateltest = new List<Uzivateltest>();
            MySqlConnection connection = new MySqlConnection("data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass);
            MySqlCommand command = new MySqlCommand("select * from "+globalVar.table_Name+".uzivatele where jmeno='" + this.textBox12.Text + "';", connection);
            MySqlDataReader darek;
            try
            {
                connection.Open();
                darek = command.ExecuteReader();
                while (darek.Read())
                {
                    uzivateltest.Add(new Uzivateltest()
                    {
                        ID = darek.GetString("ID"),
                        jmeno = darek.GetString("jmeno"),
                        heslo = darek.GetString("heslo"),
                        pravomoce = darek.GetInt32("pravomoce"),
                    });
                }

            }

            catch
            {
                throw;
            }
            finally
            {
                connection.Close();
            }
            ///
            if (uzivateltest[0].pravomoce == 2)
            {
                var val = this.textBox11.Text;
                if (String.IsNullOrEmpty(val))
                {
                    MessageBox.Show("Nezadali jste ID záznamu pro odstranění");
                }
                else
                {
                    string constring = "data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass;
                    string Query = "delete from "+globalVar.table_Name+".data where ID='" + this.textBox11.Text + "';";
                    MySqlConnection conDataBase = new MySqlConnection(constring);
                    MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
                    MySqlDataReader myReader;
                    try
                    {
                        conDataBase.Open();
                        myReader = cmdDataBase.ExecuteReader();
                        MessageBox.Show("Odstraněno!");
                        while (myReader.Read())
                        {

                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        conDataBase.Close();
                    }
                }
            }
            else 
            {
                MessageBox.Show("Na toto nemáte oprávnění!");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string constring = "data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass;
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(" select ID,prvek,beta,alpha,ro,kod from " + globalVar.table_Name + ".data ORDER BY ID", conDataBase);

            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();
                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        //////
        private void button9_Click(object sender, EventArgs e)
        {
            string constring = "data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass;
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(" select ID,prvek,beta,alpha,ro,kod from " + globalVar.table_Name + ".data ORDER BY ID", conDataBase);

            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                dbdataset2 = new DataTable();
                sda.Fill(dbdataset2);
                BindingSource bSource = new BindingSource();
                bSource.DataSource = dbdataset2;
                dataGridView2.DataSource = bSource;
                sda.Update(dbdataset2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /////////
        private void button10_Click(object sender, EventArgs e)
        {
            string constring = "data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass;
            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(" select ID,prvek,beta,alpha,ro,kod from " + globalVar.table_Name + ".data ORDER BY ID", conDataBase);

            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();
                bSource.DataSource = dbdataset;
                dataGridView3.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView3.Rows[e.RowIndex];

                textBox11.Text = row.Cells["ID"].Value.ToString();
                textBox10.Text = row.Cells["prvek"].Value.ToString();
                textBox9.Text = row.Cells["beta"].Value.ToString();
                textBox8.Text = row.Cells["alpha"].Value.ToString();
                textBox7.Text = row.Cells["ro"].Value.ToString();
                textBox6.Text = row.Cells["kod"].Value.ToString();
            }
        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {
            DataView DV = new DataView(dbdataset2);
            DV.RowFilter = string.Format("prvek LIKE '%{0}%'", textBox13.Text);
            dataGridView2.DataSource = DV;
        }

        private void button11_Click(object sender, EventArgs e)
        {

            // hustota
            double teplota = 0;
            double vyska = 0;
            double sirka = 0;
            double delka = 0;

            double.TryParse(this.teplota.Text, out teplota);
            double.TryParse(this.vyska.Text, out vyska);
            double.TryParse(this.sirka.Text, out sirka);
            double.TryParse(this.delka.Text, out delka);

            double vysledek1 = ro * (1 - beta * (20 - teplota));
            this.zmena_hustoty.Text = vysledek1.ToString("#0.00");
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string beta_ne;
            string alpha_ne;
            string ro_ne;
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView2.Rows[e.RowIndex];

                prvek.Text = row.Cells["prvek"].Value.ToString();
                beta_ne = row.Cells["beta"].Value.ToString();
                alpha_ne = row.Cells["alpha"].Value.ToString();
                ro_ne = row.Cells["ro"].Value.ToString();

                double.TryParse(beta_ne, out beta);
                double.TryParse(alpha_ne, out alpha);
                double.TryParse(ro_ne, out ro);
            }
        }

        }
    }
