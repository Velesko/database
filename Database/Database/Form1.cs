﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;

namespace Database
{
    public partial class Form1 : Form
    {
        /**
         * Definování globálních proměnných 
         * 
         **/
        public static class globalVar
        {
            public const string db_Name = "localhost";
            public const string table_Name = "testtor";
            public const string user_Name = "root";
            public const string user_Pass = "toor";
        }

        public Form1()
        {
            InitializeComponent();
            textBox2.MaxLength = 15;
        }
        
        private void prihlas_Click(object sender, EventArgs e)
        {
                try
                {
                    string myConnection = "data source=" + globalVar.db_Name + ";username= " + globalVar.user_Name + ";password=" + globalVar.user_Pass;
                    MySqlConnection myConn = new MySqlConnection(myConnection);
                    MySqlCommand SelectCommand = new MySqlCommand("select * from "+ globalVar.table_Name +".uzivatele where jmeno='" + this.textBox1.Text + "'and heslo='" + md5(this.textBox2.Text) + "' ;", myConn);

                    MySqlDataReader myReader;
                    myConn.Open();
                    myReader = SelectCommand.ExecuteReader();
                    int count = 0;
                    while (myReader.Read())
                    {
                        count = count + 1;
                    }
                    if (count == 1)
                    {
                        var secform = new Form2(textBox1.Text); //předej do druhého formu proměnnou z textboxu 1
                        secform.Owner = this;
                        secform.Show();
                        this.Hide();
                    }
                        //další chybové hlášky
                    else if (count > 1)
                    {
                        MessageBox.Show("Více než jeden výsledek, přístup odepřen!");
                    }
                    else
                    {
                        MessageBox.Show("Špatně zadané jméno nebo heslo");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                textBox2.Text = "";

        }
    
        // kódování do MD5
        private string md5(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        private void ukoncit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
